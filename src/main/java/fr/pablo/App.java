package fr.pablo;

import fr.pablo.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory( "default" );
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //begin
        ProdType Typefood = ProdType.FOOD;
        ProdType TypeCleaning = ProdType.CLEANING;
        ProdType TypeAccessory = ProdType.ACCESSORY;
        FishLivEnv freshWater = FishLivEnv.FRESH_WATER;
        FishLivEnv seaWater = FishLivEnv.SEA_WATER;
        Address adresse1 = new Address("4", "rue du canada", "14320", "May sur Orne");
        Address adresse2 = new Address("101", "bd de verdun", "35000", "Rennes");
        Address adresse3 = new Address("21", "Rue du bignion", "35000", "Rennes");
        em.persist(adresse1);
        em.persist(adresse2);
        em.persist(adresse3);
        PetStore petStore1 = new PetStore("Store1", "henry", adresse1);
        PetStore petStore2 = new PetStore("Store2", "thomas", adresse2);
        PetStore petStore3 = new PetStore("Store3", "alex", adresse3);
        Product product1 = new Product("BOUF", "Bouffe", Typefood, 2.99);
        Product product2 = new Product("PAN", "panier", TypeCleaning, 5.99);
        Product product3 = new Product("GAT", "Gateau", TypeAccessory, 19.99);
        em.persist(product1);
        em.persist(product2);
        em.persist(product3);
        em.persist(petStore1);
        em.persist(petStore2);
        em.persist(petStore3);
        Fish fish1 = new Fish(LocalDate.of(2022, 5, 8), "Red", seaWater);
        fish1.setPetStore(petStore1);
        Cat cat1 = new Cat(LocalDate.of(2022, 1, 2), "Red", "CHAT0005");
        cat1.setPetStore(petStore2);
        Cat cat2 = new Cat(LocalDate.of(2022, 8, 9), "Red", "CHAT0008");
        cat2.setPetStore(petStore2);
        em.persist(fish1);
        em.persist(cat1);
        em.persist(cat2);
        em.getTransaction().commit();
        PetStore petStore4 =  em.find(PetStore.class, 1L);
        System.out.println(petStore4.getAnimals());
        em.close();
        emf.close();
    }
}
