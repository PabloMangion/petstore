package fr.pablo.entity;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class Cat extends Animal {

    private String chipId;

    public Cat() {
    }

    public Cat(String chipId) {
        this.chipId = chipId;
    }

    public Cat(LocalDate birth, String color, String chipId) {
        super(birth, color);
        this.chipId = chipId;
    }

    public String getChipId() {
        return chipId;
    }

    public void setChipId(String chipId) {
        this.chipId = chipId;
    }

}
