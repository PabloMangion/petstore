package fr.pablo.entity;

public enum ProdType {
    FOOD, ACCESSORY, CLEANING;
}
